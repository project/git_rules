<?php

/**
 * @file
 * This file contains actions, conditions and events for git.
 */

/**
 * @file
 * This file contains the actions and events.
 */
function git_rules_rules_event_info() {
  return array(
    'git_rules_git_post_receive' => array(
      'label' => t('After receiving a git post receive'),
      'module' => 'Git Rules',
      'arguments' => array(
        'repository' => array(
          'type' => 'string',
          'label' => t('Repository name'),
        ),
        'repository_path' => array(
          'type' => 'string',
          'label' => t('Repository path'),
        ),
      ),
    ),
    'git_rules_git_post_receive_commit' => array(
      'label' => t('After receiving a git post receive commit'),
      'module' => 'Git Rules',
      'arguments' => array(
        'id' => array(
          'type' => 'string',
          'label' => t('The commit id'),
        ),
        'timestamp' => array(
          'type' => 'date',
          'label' => t('Commit date'),
        ),
        'author_email' => array(
          'type' => 'string',
          'label' => t('Author Email'),
        ),
        'author_name' => array(
          'type' => 'string',
          'label' => t('Author Name'),
        ),
        'message' => array(
          'type' => 'string',
          'label' => t('Commit message'),
        ),
        'repository' => array(
          'type' => 'string',
          'label' => t('The repository name'),
        ),
        'repository_path' => array(
          'type' => 'string',
          'label' => t('Repository path'),
        ),
      ),
    ),
  );

}

function git_rules_rules_condition_info() {
  return array(
    'git_rules_repository_exists' => array(
      'label' => t('Evaluate if a repository exists'),
      'module' => 'Git Rules',
      'arguments' => array(
        'repository' => array(
          'type' => 'string',
          'label' => t('Repository'),
          'description' => t('The repository to check for.'),
          ),
        'repo_path' => array(
          'type' => 'string',
          'label' => t('Repository path'),
          'description' => t('The path to look for the repository in.')
        ),
      ),
    ),
  );
}

/**
 * Implementation of hook_rules_action_info().
 */
function git_rules_rules_action_info() {
  $actions = array();

  $actions['git_rules_clone_repository'] = array(
      'label' => t('Clone a repository'),
      'module' => 'Git Rules',
      'arguments' => array(
        'external_path' => array(
          'type' => 'string',
          'label' => t('External repository path'),
          'description' => t('The url of the repository.'),
        ),
        'local_path' => array(
          'type' => 'string',
          'label' => t('Local Repository path'),
          'description' => t('The path where the repository should be cloned'),
        ),
        'bare' => array(
          'type' => 'boolean',
          'label' => t('Bare'),
          'description' => t('Equal to the --bare property.'),
        ),
      ),
    );
    $actions['git_rules_fetch_repository'] = array(
      'label' => t('Fetch repository changes in a public repository'),
      'module' => 'Git Rules',
      'arguments' => array(
        'external_path' => array(
          'type' => 'string',
          'label' => t('External repository path or the remote to fetch, and refspec'),
          'description' => t('The url of the repository.'),
          ),
         'local_path' => array(
          'type' => 'string',
          'label' => t('Local Repository path'),
          'description' => t('The path of the repository. It must exist!'),
          ),
      ),
    );
    $actions['git_rules_create_remote'] = array(
      'label' => t('Create a Remote in a repository.'),
      'module' => 'Git Rules',
      'arguments' => array(
        'external_path' => array(
          'type' => 'string',
          'label' => t('External repository path'),
          'description' => t('The url of the repository.'),
          ),
         'local_path' => array(
            'type' => 'string',
            'label' => t('Local Repository path'),
            'description' => t('The path of the repository. It must exist!'),
          ),
          'name' => array(
            'type' => 'string',
            'label' => t('The Remote name.'),
            'description' => t('The remote name. If it already exists, this will result in errors.'),
          ),
          'bare' => array(
            'type' => 'boolean',
            'label' => t('Bare'),
            'description' => t('Equal to the --bare property.'),
          ),
      ),
    );
    $actions['git_rules_remove_remote'] = array(
      'label' => t('Remove a Remote in a repository.'),
      'module' => 'Git Rules',
      'arguments' => array(
         'local_path' => array(
            'type' => 'string',
            'label' => t('Local Repository path'),
            'description' => t('The path of the repository. It must exist!'),
          ),
          'name' => array(
            'type' => 'string',
            'label' => t('The Remote name.'),
            'description' => t('The remote name. The remote must exist.'),
          ),
          'bare' => array(
            'type' => 'boolean',
            'label' => t('Bare'),
            'description' => t('Equal to the --bare property.'),
          ),
      ),
    );
    $actions['git_rules_init_repository'] = array(
      'label' => t('Init a repository'),
      'module' => 'Git Rules',
      'arguments' => array(
        'local_path' => array(
          'type' => 'string',
          'label' => t('Local Repository path'),
          'description' => t('The path where the repository should be inited'),
        ),
        'bare' => array(
          'type' => 'boolean',
          'label' => t('Bare'),
          'description' => t('Equal to the --bare property.'),
        ),
      ),
    );
    return $actions;
}

/**
 * Check that a repository exists.
 * @param string $repository
 * @param string $repo_path
 * @return true if the path is a repo, false otherwise.
 */
function git_rules_repository_exists($repository, $repo_path)  {
  return is_dir($repo_path);
}

/**
 * Initiate a repository.
 * @param string $local_path the local path to the repository.
 * @param bool $bare if the repository should be inited as a bare repo.
 */
function git_rules_init_repository($local_path, $bare) {
  $cmd = 'mkdir ' . $local_path . '; cd ' . $local_path .'; git init';
  if ($bare) {
    $cmd .= ' --bare ';
  }
  git_rules_command_queue_add($cmd);
}

/**
 * Clone a repository.
 * @param string $external_path
 * @param string $local_path
 * @param boolean $bare
 */
function git_rules_clone_repository($external_path, $local_path, $bare = 0) {
  $cmd = 'git clone ';
  // Add the bare parameter.
  if ($bare) {
    $cmd .= '--bare ';
  }
  $cmd .= $external_path . ' ' . $local_path;
  git_rules_command_queue_add($cmd);
}

/**
 * Create a remote in a repository.
 * @param string $external_path
 * @param string $local_path
 * @param string $remote_name
 * @param string $bare
 */
function git_rules_create_remote($external_path, $local_path, $remote_name, $bare) {
  $cmd = 'cd ' . $local_path . '; git ';
  // Add the bare parameter if needed.
  if ($bare) {
    $cmd .= '--bare ';
  }
  $cmd .= 'remote add ' . $remote_name . ' ' . $external_path;
  git_rules_command_queue_add($cmd);
}
/**
 * Remove a remote from a repository.
 * @param string $local_path
 * @param string $remote_name
 * @param boolean $bare
 */
function git_rules_remove_remote($local_path, $remote_name, $bare) {
  $cmd = 'cd ' . $local_path . '; git ';
  // Add the bare parameter if needed.
  if ($bare) {
    $cmd .= '--bare ';
  }
  $cmd .= 'remote rm ' . $remote_name;
  git_rules_command_queue_add($cmd);
}

/**
 * Fetch data from an external repository.
 * @param string $external_path the path to the repo. The refspec can also be specified.
 * @param string $local_path
 */
function git_rules_fetch_repository($external_path, $local_path) {
  $cmd = 'cd ' . $local_path . '; git fetch ' . $external_path;
  git_rules_command_queue_add($cmd);
}